const findTwoSumHashMap = (nums, target) => {
  const hashMap = {};

  for (let p = 0; p < nums.length; p++) {
    const currMapVal = hashMap[nums[p]];

    if (currMapVal >= 0) {
      return [currMapVal, p];
    } else {
      const numberToFind = target - nums[p];
      hashMap[numberToFind] = p;
    }
  }

  return null;
};

console.log(findTwoSumHashMap([1, 3, 7, 9, 2], 11));
