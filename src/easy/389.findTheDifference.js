/**
 * @param {string} s
 * @param {string} t
 * @return {string}
 */
const findTheDifference = (s, t) => {
  const sCharCodeSum = [...s]
    .map((el) => el.charCodeAt(0))
    .reduce((prev, curr) => prev + curr, 0);

  const tCharCodeSum = [...t]
    .map((el) => el.charCodeAt(0))
    .reduce((prev, curr) => prev + curr, 0);

  const result = String.fromCharCode(tCharCodeSum - sCharCodeSum);

  console.log({ sCharCodeSum, tCharCodeSum, result });
};

const s = 'abcd';
const t = 'abcde';

const res = findTheDifference(s, t);
// console.log(res);
