/**
 * @param {number[]} nums
 * @return {boolean}
 */
const containsDuplicate = (nums) => {
  const checked = {};

  for (let i = 0; i < nums.length; i += 1) {
    if (checked[nums[i]] !== undefined) {
      return true;
    }
    checked[nums[i]] = nums[i];
  }

  return false;
};

const testCase = [1, 1, 1, 3, 3, 4, 3, 2, 4, 2]; // true
const testCase2 = [1, 2, 3, 4]; // false
const testCase3 = [1, 2, 3, 1]; // true
const testCase4 = [0, 4, 5, 0, 3, 6]; // true
console.log(containsDuplicate(testCase));
console.log(containsDuplicate(testCase2));
console.log(containsDuplicate(testCase3));
console.log(containsDuplicate(testCase4));
