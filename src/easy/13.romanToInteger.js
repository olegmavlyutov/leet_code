function romanToIntegerFirst(s) {
  const romanMap = {
    M: 1000,
    D: 500,
    C: 100,
    L: 50,
    X: 10,
    V: 5,
    I: 1,
  };

  let total = 0;
  let i = 0;

  while (i < s.length) {
    // пока до конца есть хотя бы 2 символа И значение i-индекса меньше чем значение i+1-индекса, например в IV -> I (1) меньше чем V (5)
    if ((i + 1 < s.length) && (romanMap[s[i]] < romanMap[s[i + 1]])) {
      // прибавляем к общей сумме разность символов, например в IX -> вычитаем из значения X (10) значение I (1)
      total += romanMap[s[i + 1]] - romanMap[s[i]];
      // сдвигаем индекс за эту подстроку
      i += 2;
    } else {
      total += romanMap[s[i]];
      i += 1;
    }
  }

  return total;
}

function romanToIntegerSubstrings(s) {
  const romanMapSubstrings = {
    I: 1,
    V: 5,
    X: 10,
    L: 50,
    C: 100,
    D: 500,
    M: 1000,
    IV: 4,
    IX: 9,
    XL: 40,
    XC: 90,
    CD: 400,
    CM: 900,
  };

  let total = 0;
  let i = 0;

  while (i < s.length) {
    // пока остается хотя бы 2 символа И искомая подстрока есть в хэш-таблице
    if ((i < s.length - 1) && (romanMapSubstrings[s.slice(i, i + 2)] !== undefined)) {
      // прибавляем значение найденной подстроки
      total += romanMapSubstrings[s.slice(i, i + 2)];
      // сдвигаем индекс за подстроку
      i += 2;
    } else {
      total += romanMapSubstrings[s[i]];
      i += 1;
    }
  }

  return total;
}

function romanToIntegerSecond(s) {
  const romanMap = {
    M: 1000,
    D: 500,
    C: 100,
    L: 50,
    X: 10,
    V: 5,
    I: 1,
  };

  // если смотреть на римские цифры справа налево, то правая часть составного числа всегда есть в его сумме:
  // 1. левая часть вычитается, если она меньше правой (IV -> вычитаем I из V, т.к. I < V)
  // 2. левая часть прибавляется, если она больше правой (VI -> прибавляем V к I, т.к. V > I)
  const last = s.length - 1;
  // сразу прибавляем к итоговой сумме значение последнего символа
  let total = romanMap[s[last]];

  // движемся по строке справа налево, пока не достигнем её нулевого элемента
  for (let i = last - 1; i >= 0; i -= 1) {
    // левый символ меньше - вычитаем его значение
    if (romanMap[s[i]] < romanMap[s[i + 1]]) {
      total -= romanMap[s[i]];
    } else {
      // левый символ больше - прибавляем его значение
      total += romanMap[s[i]];
    }
  }

  return total;
}

const testCase = 'MMMCMXCIX'; // 3999 => 1000 + 1000 + 1000 + 900 + 90 + 9
console.log(romanToIntegerFirst(testCase));
console.log(romanToIntegerSubstrings(testCase));
console.log(romanToIntegerSecond(testCase));
