const singleNumber = (nums) => {
  let mark = 0;

  // работает на принципе битового сдвига - если дважды передать одно и то же число, то в отввете будет 0
  // например в [4, 1, 2, 1, 2] от 0 как инициатора => 0 -> 4 -> 5 -> 7 -> 6 -> 4 => вернет 4
  for (let i = 0; i < nums.length; i += 1) {
    mark ^= nums[i];
  }

  return mark;
};

const testCase = [2, 2, 1]; // 1
const testCase2 = [4, 1, 2, 1, 2]; // 4
const testCase3 = [1]; // 1
console.log(singleNumber(testCase));
console.log(singleNumber(testCase2));
console.log(singleNumber(testCase3));
