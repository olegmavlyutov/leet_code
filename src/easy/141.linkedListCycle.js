/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 */
const hasCycle = (head) => {
  // изначально медленный и быстрый указатели смотрят на начало связного списка
  let slowPointer = head;
  let fastPointer = head;

  // пока существует текущий и следующий элемент списка повторяем в цикле
  while (fastPointer && fastPointer.next) {
    // медленный делает один шаг
    slowPointer = slowPointer.next;
    // быстрый делает два шага
    fastPointer = fastPointer.next.next;

    // если медленный и быстрый указатели совпадут, значит мы попали в цикл
    if (slowPointer === fastPointer) {
      return true;
    }
  }

  // если дошли до конца и быстрый с медленным не встретились - цикла не было
  return false;
};
