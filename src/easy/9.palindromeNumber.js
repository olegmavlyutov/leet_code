function isPalindromeNumberTwoPointers(number) {
  console.time('isPalindromeNumberTwoPointers');
  const str = number.toString();
  let leftPointer = 0;
  let rightPointer = str.length - 1;

  while (leftPointer < rightPointer) {
    if (str[leftPointer] !== str[rightPointer]) {
      return false;
    }

    leftPointer += 1;
    rightPointer -= 1;
  }

  console.timeEnd('isPalindromeNumberTwoPointers');
  return true;
}

function isPalindromeRevertNumber(number) {
  console.time('isPalindromeRevertNumber');
  let _number = number;
  let revertedNumber = 0;

  if (_number < 0 || (_number % 10 === 0 && _number !== 0)) {
    return false;
  }

  while (_number > revertedNumber) {
    revertedNumber = revertedNumber * 10 + (_number % 10);
    _number = Math.floor(_number / 10);
  }

  console.timeEnd('isPalindromeRevertNumber');
  return (_number === revertedNumber) || (_number === Math.floor(revertedNumber / 10));
}

const testNumber = 122445535544221;

console.log(isPalindromeNumberTwoPointers(testNumber));
console.log(isPalindromeRevertNumber(testNumber));

