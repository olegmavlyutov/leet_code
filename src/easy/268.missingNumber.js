const missingNumberBrute = (nums) => {
  const numsHashMap = {};
  const rangeNums = [];

  // какие элементы должны быть
  for (let i = 0; i < nums.length + 1; i += 1) {
    rangeNums.push(i);
  }

  // создаём хеш-таблицу для элементов, которые есть
  for (let j = 0; j < nums.length; j += 1) {
    numsHashMap[nums[j]] = nums[j];
  }

  // проверяем какой элемент должен быть, но отсутствует
  for (let k = 0; k < rangeNums.length; k += 1) {
    if (numsHashMap[k] === undefined) {
      return rangeNums[k];
    }
  }

  return -1;
};

const missingNumbersWithArithmeticalProgression = (nums) => {
  const n = nums.length;
  const sumOfNumsElements = nums.reduce((prev, curr) => prev + curr, 0);

  // вычитаем из предполагаемой суммы ряда (арифметическая прогрессия в нашем случае) сумму элементов, которая дана
  // например, в [3, 0, 1] -> n = 3, значит мы ищем сумму элементов [0, 1, 2, 3] - это 6; вычитаем из него сумму [3, 0, 1] - это 4;
  // 6 - 4 = 2, это и есть наш ответ
  return ((n + 1) * n) / 2 - sumOfNumsElements;
};

const testCase = [3, 0, 1]; // 2 (n = 3, т.к. length = 3; ищем какого числа нет в [0,..,3])
const testCase2 = [0, 1]; // 2 (n = 2, т.к. length = 2; ищем какого числа нет в [0,..,2])
const testCase3 = [9, 6, 4, 2, 3, 5, 7, 0, 1]; // 8 (n = 9, т.к. length = 9; ищем какого числа нет в [0,..,9])
console.log(missingNumberBrute(testCase));
console.log(missingNumberBrute(testCase2));
console.log(missingNumberBrute(testCase3));
console.log(missingNumbersWithArithmeticalProgression(testCase));
console.log(missingNumbersWithArithmeticalProgression(testCase2));
console.log(missingNumbersWithArithmeticalProgression(testCase3));
