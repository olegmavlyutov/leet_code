/**
 * @param {number[]} nums
 * @return {number[]}
 */
const findDisappearedNumbersBrute = (nums) => {
  const numbersThatShouldBe = [];
  const numsMap = {};
  const result = [];

  for (let i = 1; i < nums.length + 1; i += 1) {
    numbersThatShouldBe.push(i);
  }

  for (let j = 0; j < numbersThatShouldBe.length; j += 1) {
    numsMap[nums[j]] = nums[j];
  }

  for (let k = 0; k < numbersThatShouldBe.length; k += 1) {
    if (numsMap[numbersThatShouldBe[k]] === undefined) {
      result.push(numbersThatShouldBe[k]);
    }
  }

  return result;
};

const findDisappearedNumbersFirst = (nums) => {
  const result = [];
  const undefinedIndexes = [];

  for (let i = 0; i < nums.length; i += 1) {
    result[nums[i] - 1] = nums[i];
  }

  for (let j = 0; j < nums.length; j += 1) {
    if (result[j] === undefined) {
      undefinedIndexes.push(j + 1);
    }
  }

  return undefinedIndexes;
};

const testCase = [4, 3, 2, 7, 8, 2, 3, 1]; // [5, 6]
const testCase2 = [1, 1]; // [2]

console.log(findDisappearedNumbersBrute(testCase));
console.log(findDisappearedNumbersBrute(testCase2));

console.log(findDisappearedNumbersFirst(testCase));
console.log(findDisappearedNumbersFirst(testCase2));
