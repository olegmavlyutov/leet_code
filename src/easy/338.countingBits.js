const countingBits = (n) => {
  const memo = [0];

  for (let i = 1; i < n + 1; i += 1) {
    memo[i] = memo[i >> 1] + (i % 2);
  }

  return memo.slice(0, n + 1);
};

const testCase = 1;
const testCase2 = 5;

console.log(countingBits(testCase));
console.log(countingBits(testCase2));
