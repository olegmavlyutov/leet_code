/**
 * Массив из целых положительных чисел.
 * Вернуть максимальную чётную сумму элементов массива
 *
 * Примеры:
 * [8, 11, 4, 1, 4] => 28 (т.к. 8 + 11 + 4 + 1 + 4, сумма всех элементов)
 * [8, 11, 4, 1, 4, 1] => 28 (т.к. 8 + 11 + 4 + 1 + 4, сумма всех элементов без 1)
 * [7, 11, 4, 1, 4] => 26 (т.к. 7 + 11 + 4 + 4, сумма элементов без 1, порядок не важен)
 */
const findMaxEvenSumOfArray = (array) => {
  const rawSum = array.reduce((prev, curr) => prev + curr, 0);
  const oddNums = array.filter((n) => n % 2 !== 0);

  if (rawSum % 2 === 0) {
    return rawSum;
  }
  return rawSum - Math.min(...oddNums);
};

console.log(findMaxEvenSumOfArray([7, 11, 4, 1, 4]));
