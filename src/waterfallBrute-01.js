const getTrappedRainwater = (heights) => {
  let totalWater = 0;

  for (let p = 0; p < heights.length; p += 1) {
    let leftP = p; let rightP = p; let maxLeft = 0; let
      maxRight = 0;

    while (leftP >= 0) {
      maxLeft = Math.max(maxLeft, heights[leftP]);
      leftP -= 1;
    }

    while (rightP < heights.length) {
      maxRight = Math.max(maxRight, heights[rightP]);
      rightP += 1;
    }

    const currentWater = Math.min(maxLeft, maxRight) - heights[p];

    if (currentWater >= 0) {
      totalWater += currentWater;
    }
  }

  return totalWater;
};

const elevationArray = [0, 1, 0, 2, 1, 0, 3, 1, 0, 1, 2]; // 8
console.log(getTrappedRainwater(elevationArray));
