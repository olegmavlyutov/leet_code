function recursiveSum(arr) {
  if (arr.length < 1) {
    return 0;
  }
  return arr[0] + recursiveSum(arr.splice(1, arr.length - 1));
}

const res = recursiveSum([2, 4, 6]);
console.log({ res });
