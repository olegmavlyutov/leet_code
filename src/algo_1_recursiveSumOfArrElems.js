function recursiveSumOfArrElems(arr) {
  if (arr.length < 1) {
    return 0;
  }
  return 1 + recursiveSumOfArrElems(arr.splice(1, arr.length - 1));
}

const res = recursiveSumOfArrElems([2, 4, 6, 8, 10]);
console.log({ res });
