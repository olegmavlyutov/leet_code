function recursiveMaxOfArrayElems(arr) {
  if (arr.length === 2) {
    return arr[0] > arr[1] ? arr[0] : arr[1];
  }
  const subMax = recursiveMaxOfArrayElems(arr.splice(1, arr.length - 1));
  return arr[0] > subMax ? arr[0] : subMax;
}

const res = recursiveMaxOfArrayElems([19, 2, 4, 6]);
console.log({ res });
