const containerBrute = (nums) => {
  let maxArea = 0;

  for (let a = 0; a < nums.length; a += 1) {
    for (let b = a + 1; b < nums.length; b += 1) {
      const height = Math.min(nums[a], nums[b]);
      const width = b - a;
      const candidateArea = height * width;

      maxArea = Math.max(maxArea, candidateArea);
    }
  }
  return maxArea;
};

console.log(containerBrute([7, 1, 2, 3, 9]));
console.log(containerBrute([]));
console.log(containerBrute([7]));
console.log(containerBrute([6, 9, 3, 4, 5, 8]));
